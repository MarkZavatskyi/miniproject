module.exports = {
  up: (queryInterface, Sequelize) => Promise.all([
    queryInterface.addColumn(
      'posts',
      'deletedAt',
      {
        type: Sequelize.DATE,
        allowNull: true
      }
    )
  ]),

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => Promise.all([
    queryInterface.removeColumn('posts', 'deletedAt')
  ])
};
