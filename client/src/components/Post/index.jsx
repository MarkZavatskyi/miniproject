import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import UpdatePost from '../UpdatePost';

import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, update, remove, toggleExpandedPost, sharePost, currentUser }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();

  const [updatePost, setUpdatePost] = useState(false);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
            { (currentUser.id !== user.id)
            || (
              <span className={styles.update_btn}>
                { updatePost
                  ? <Icon name="close" className={styles.closeUpdateForm} onClick={() => setUpdatePost(!updatePost)} />
                  : <Icon name="pen square" className={styles.update} onClick={() => setUpdatePost(!updatePost)} /> }
                <Icon name="trash" className={styles.remove} onClick={() => remove(id)} />
              </span>
            )}
          </span>
        </Card.Meta>
        <Card.Description>
          { updatePost ? <UpdatePost post={post} update={update} closeForm={() => setUpdatePost(false)} /> : body }
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  remove: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  currentUser: PropTypes.objectOf(PropTypes.any).isRequired
};

const mapStateToProps = ({ profile }) => ({
  currentUser: profile.user
});

export default connect(mapStateToProps)(Post);
