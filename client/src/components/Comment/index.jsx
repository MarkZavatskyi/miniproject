import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment: { id, body, createdAt, user, likeCount, dislikeCount },
  likeComment,
  dislikeComment
}) => (
  <CommentUI className={styles.comment}>
    {console.log(likeCount, dislikeCount)}
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      <CommentUI.Actions style={{ display: 'inline-block', position: 'absolute', right: '5px', top: '0' }}>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
      </CommentUI.Actions>
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
