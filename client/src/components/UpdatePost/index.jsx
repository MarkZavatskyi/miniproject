import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import { Form, Button, Icon, Image } from 'semantic-ui-react';
import styles from '../AddPost/styles.module.scss';

const UpdatePost = ({ post, update, closeForm }) => {
  const {
    id,
    image,
    body
  } = post;

  const [postBody, setPostBody] = useState(body);
  const [postImage, setPostImage] = useState(image);
  const [isUploading, setIsUploading] = useState(false);

  const uploadImage = file => imageService.uploadImage(file);

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setPostImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await update(id, { imageId: postImage?.imageId, postBody });
    setPostBody('');
    setPostImage(undefined);
    closeForm();
  };

  return (
    <Form onSubmit={handleUpdatePost}>
      <Form.TextArea
        name="body"
        value={postBody}
        onChange={e => setPostBody(e.target.value)}
      />
      {postImage?.imageLink && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={postImage?.imageLink} alt="post" />
        </div>
      )}
      <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
        <Icon name="image" />
        Attach image
        <input name="image" type="file" onChange={handleUploadFile} hidden />
      </Button>
      <Button floated="right" color="blue" type="submit">Save</Button>
    </Form>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  update: PropTypes.func.isRequired,
  closeForm: PropTypes.func.isRequired
};

export default UpdatePost;
