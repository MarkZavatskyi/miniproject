import * as commentService from 'src/services/commentService';
import { SET_EXPANDED_POST_COMMENTS } from 'src/containers/Thread/actionTypes';

const setExpandedPostComments = comments => ({
  type: SET_EXPANDED_POST_COMMENTS,
  comments
});

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.likeComment(commentId);
  const diff = id ? 1 : -1;

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff,
    dislikeCount: id && createdAt !== updatedAt ? Number(comment.dislikeCount) - diff : Number(comment.dislikeCount)
  });

  const { posts: { expandedPost: { comments } } } = getRootState();
  const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));
  dispatch(setExpandedPostComments(updatedComments));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.dislikeComment(commentId);
  const diff = id ? 1 : -1;

  const mapLikes = comment => ({
    ...comment,
    likeCount: id && createdAt !== updatedAt ? Number(comment.likeCount) - diff : Number(comment.likeCount),
    dislikeCount: Number(comment.dislikeCount) + diff
  });

  const { posts: { expandedPost: { comments } } } = getRootState();
  const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));
  dispatch(setExpandedPostComments(updatedComments));
};
